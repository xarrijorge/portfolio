import React from "react"

import Layout from "../components/layout"
import HeadHero from "../components/headHero"
import About from "../components/about"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
    <HeadHero />
    <About />
    <footer>
      © {new Date().getFullYear()}{" "}
      <a href="https://xarrijorge.com">xarrijorge.com</a>
      <a href="https://xarrijorge.com">xarrijorge.com</a>
      <a href="https://gitlab.com/xarrijorge">
        <i />
      </a>
      <a href="https://xarrijorge.com">xarrijorge.com</a>
    </footer>
  </Layout>
)

export default IndexPage
