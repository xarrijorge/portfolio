import React from 'react';

import Arrow from './arrowDown';

export default () =>(
  <div className="base head-hero">
    <div className="content">
      <div className="hero-title">
        <h1>Xarri Jorge</h1>
        <span className="hero-div1"></span>
        <span className="hero-div2"></span>
        <span className="hero-sub">Frontend<br/> Developer</span>
      </div>
      <p className="hero-desc">Everyone deserves Beauty and Innovation<br/> 
      ... I'm delivering just that! </p>
    <Arrow/>
    </div>
  </div>
)
