import React from 'react';
import image from '../images/xarriImage.jpg';

export default ()=>(
  <div className="base about">
      <div className="content">
        <section></section>
          <p>I'm a Frontend UI/UX designer. I love what I do and I especially enjoy the process of taking a project from concept to completion. Because of the love of the process, I've have experience in backend technologies like Node and Rails  and the stacks that come with them.</p> 
      </div>
      <div className="graphic">
        <img src={image} alt="Xarri standing"/>
      </div>
    </div>
)
