import React from 'react';
import {Link, animateScroll as scroll, scrollSpy} from 'react-scroll';

export default () =>(
  <Link activeClass="active" to="about" smooth={true} spy={true} duration={500}offset={-50}>
    <div className="arrow"></div>
  </Link>
)
